from django.contrib import admin
from django.urls import path, include
from project import urls
from chechahin import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(urls)),
    path('summernote/', include('django_summernote.urls')),  # django_summernote URLS
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
