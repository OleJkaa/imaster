from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from project.models import *

# class ModelAdmin(admin.ModelAdmin):
#     list_display = ('model', 'category_device', 'index')
#     list_filter = ('category_device',)
#     prepopulated_fields = {"slug": ('model',)}

class ModelAdmin2(SummernoteModelAdmin):
    list_display = ('model', 'category_device', 'index')
    list_filter = ('category_device',)
    prepopulated_fields = {"slug": ('model',)}
    summer_note_fields = ('title','description','problems')


class PriceOfWorkAdmin(admin.ModelAdmin):
    list_display = ('type_of_work', 'model', 'index', 'price')
    list_filter = ('model', 'type_of_work')

class TextBodyAdmin(SummernoteModelAdmin):
    summer_note_fields = ('description',)
    list_display = ('title', 'description')
    list_filter = ('title', 'description')

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ('category',)}

admin.site.register(TextBody,TextBodyAdmin)
admin.site.register(TypeOfWork)
admin.site.register(PriceOfWork,PriceOfWorkAdmin)
admin.site.register(Model, ModelAdmin2)
admin.site.register(Category, CategoryAdmin)