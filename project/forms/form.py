from bootstrap4.widgets import RadioSelectButtonGroup
from django.forms import forms
from django.forms import fields

class MyForm(forms.Form):
    media_type = fields.ChoiceField(
        help_text="Select the order type.",
        required=True,
        label="Order Type:",
        widget=RadioSelectButtonGroup,
        choices=((1, 'Vinyl'), (2, 'Compact Disc')),
        initial=1,
    )