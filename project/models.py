from django.db import models

app_label = 'project'


def category_picture_directory_path(instance, filename):
    return 'category_images/{0}/{1}'.format(instance.category.replace(' ', ''), filename)


def model_picture_directory_path(instance, filename):
    return 'models_image/{0}/{1}'.format(instance.model.replace(' ', ''), filename)


def type_of_price_picture_directory_path(instance, filename):
    return 'type_of_price/typework/{}'.format(filename)


class Category(models.Model):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    category = models.CharField(max_length=128, verbose_name='Категория')
    image = models.ImageField(verbose_name='Изображение (рекомендуется 600х800)',
                              upload_to=category_picture_directory_path)
    slug = models.SlugField(verbose_name='Slug')

    def __str__(self):
        return self.category

    def get_absolute_url(self):
        return "/{}".format(self.slug)


class Model(models.Model):
    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'
        ordering = ['index']

    index = models.IntegerField(verbose_name='Порядковый номер')
    model = models.CharField(max_length=100, verbose_name='Модель (текст так же отображается на главной странице)')
    category_device = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category_device',
                                        verbose_name='Категория')
    title = models.TextField(verbose_name='Заголовок описания на странице ремонта модели',blank=True,null=True)
    description = models.TextField(verbose_name='Описание на странице ремонта модели',blank=True,null=True)
    slug = models.SlugField(max_length=100, verbose_name='Slug')
    problems = models.TextField(max_length=5000, verbose_name='Самыми распространенными причинами, по которым клиенты обращаются в наш сервисный центр (Каждая причина с новой строки)',  blank=True, null=True)
    image = models.ImageField(verbose_name='Изображение', upload_to=model_picture_directory_path)

    def __str__(self):
        return self.model

    def get_absolute_url(self):
        return "/{}/{}".format(self.category_device.slug, self.slug)

    @property
    def return_problems(self):
        if self.problems != None and len(self.problems) > 0 and '\n' in self.problems:
            return list(self.problems.split('\n'))
        else:
            return [
                'неисправности, возникшие в результате неудачного падения;',
                'Различные механические повреждения в виде трещин,сколов;',
                'Длительное пребывание в воде;',
                'Неаккуратное обращение с устройством.'
            ]


class TypeOfWork(models.Model):
    class Meta:
        verbose_name = 'Тип работы'
        verbose_name_plural = 'Типы работ'

    type_of_work = models.CharField(max_length=500, verbose_name='тип работы')
    image = models.ImageField(verbose_name='Изображение', upload_to=type_of_price_picture_directory_path)

    def __str__(self):
        return self.type_of_work


class PriceOfWork(models.Model):
    class Meta:
        verbose_name = 'Цена за работу'
        verbose_name_plural = 'Цены за работу'
        ordering = ['index']

    index = models.IntegerField(verbose_name='Порядковый номер')
    model = models.ForeignKey(Model, verbose_name='Модель девайса', on_delete=models.CASCADE, related_name='types_work')
    type_of_work = models.ForeignKey(TypeOfWork, on_delete=models.CASCADE, verbose_name='Тип работы')
    price = models.IntegerField(verbose_name='Цена за ремонт')
    description = models.TextField(max_length=5000, verbose_name='Описание')

    def __str__(self):
        return '{} - {} - {}р'.format(self.type_of_work, self.model, self.price)


class TextBody(models.Model):
    class Meta:
        verbose_name = 'Текст в блоке "Как мы работаем"'
        verbose_name_plural = 'Текст в блоке "Как мы работаем"'

    title = models.CharField('Заголовок в блоке', max_length=1000)
    description = models.TextField(verbose_name='Описание', max_length=5000)

    def __str__(self):
        return '{}'.format(self.title)