ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map('map', {
            center: [55.79, 37.64],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });

    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    myMap.geoObjects.add(objectManager);

    var yandex_data = document.location.protocol + '//' + document.location.host + '/static/js/yandexmap/data.json'

    $.ajax({
        url: yandex_data
    }).done(function(data) {
        objectManager.add(data);
    });

}