from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import RedirectView

from chechahin import settings
from project.views import views

urlpatterns = [
    path('', views.HomePage.as_view(), name='home_page'),
    url(r'^send_mail', views.send_mail, name='send_mail'),
    path('detail', views.DetailModel.as_view(), name='detail_model'),
    url(r'^(?P<slug>[-_\w]+)$', views.CategoryView.as_view(), name='CategoryView'),
    url(r'^(?P<category>[-_\w]+)/(?P<slug>[-_\w]+)$', views.ModelView.as_view(), name='ModelView'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico', permanent=True)),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
