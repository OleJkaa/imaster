from django.core.mail import EmailMessage
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from django.views.generic.base import TemplateView

from project.models import Model, Category, TextBody


class HomePage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['MacBook'] = Category.objects.get(category='MacBook')
        context['iPad'] = Category.objects.get(category='iPad')
        context['iPhone'] = Category.objects.get(category='iPhone')
        context['iWatch'] = Category.objects.get(category='iWatch')
        context['iMac'] = Category.objects.get(category='iMac')
        context['MacPro'] = Category.objects.get(category='Mac Pro')
        context['MacMini'] = Category.objects.get(category='Mac mini')
        context['iPod'] = Category.objects.get(category='iPod')
        context['title'] = 'iMaster сеть специализированных сервисных центров Apple в Москве'
        context['block_text'] = TextBody.objects.all()[:3]
        return context

def send_mail(request):
    if request.POST:
        name = request.POST.get('name','Без имени')
        phone = request.POST.get('phone','Без номера')
        title = request.POST.get('title','Без описания')
        email = EmailMessage('Новое обращение', 'Имя: {}. Телефон: {}. Тип обращения: {}'.format(name,phone,title), to=['zakaz@psfd.ru', 'Klim8004@yandex.ru', '7447740@mail.ru'])
        email.send()
        if email:
            return HttpResponse(status=200)
    return HttpResponse(status=400)

class DetailModel(TemplateView):
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = Model.objects.get(id=1)
        return context

class CategoryView(DetailView):
    template_name = 'category.html'
    slug_url_kwarg = 'slug'
    model = Category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Выберите свой {}'.format(self.object)
        return context

class ModelView(DetailView):
    template_name = 'model.html'
    slug_url_kwarg = 'slug'
    model = Model

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = '{} iMaster'.format(self.object)
        context['block_text'] = TextBody.objects.all()[:3]
        return context